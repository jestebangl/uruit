﻿namespace UruIT.GameOfDrones.Service.DependencyResolution.InfraestructureInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using UruIT.GameOfDrones.Domain.Interfaces;
    using UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces;
    using UruIT.GameOfDrones.Infraestructure.DataAccess.Dao;
    using UruIT.GameOfDrones.Infraestructure.DataAccess.DataAccess;

    public class DataAccessInstaller : IWindsorInstaller
    {
        public string ConnectionString { get; set; }

        public DataAccessInstaller(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IDataAccessObject>().ImplementedBy<DataAccessObject>().Named("Dao")
                    .DependsOn(Dependency.OnValue("ConnectionString", this.ConnectionString))
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IUserDac>().ImplementedBy<UserDac>().Named("UserDac")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IMovementDac>().ImplementedBy<MovementDac>().Named("MovementDac")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IGamePlayDac>().ImplementedBy<GamePlayDac>().Named("GamePlayDac")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IGameDronRulesDac>().ImplementedBy<GameDronRulesDac>().Named("GameDronRulesDac")
                    .LifeStyle.HybridPerWebRequestPerThread()


                );
        }
    }
}
