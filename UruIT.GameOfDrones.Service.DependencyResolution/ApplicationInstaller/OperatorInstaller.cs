﻿namespace UruIT.GameOfDrones.Service.DependencyResolution.ApplicationInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using UruIT.GameOfDrones.Application.Operator.Application;
    using UruIT.GameOfDrones.Application.Operator.Helper;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;

    public class OperatorInstaller : IWindsorInstaller
    {
        public string ConnectionString { get; set; }

        public OperatorInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                 Component.For<IMovementHelper>().ImplementedBy<MovementHelper>().Named("MovementHelper")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                 Component.For<IMovementApplication>().ImplementedBy<MovementApplication>().Named("MovementApplication")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                 Component.For<IUserHelper>().ImplementedBy<UserHelper>().Named("UserHelper")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                 Component.For<IUserApplication>().ImplementedBy<UserApplication>().Named("UserApplication")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                 Component.For<IGamePlayHelper>().ImplementedBy<GamePlayHelper>().Named("GamePlayHelper")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                 Component.For<IGamePlayApplication>().ImplementedBy<GamePlayApplication>().Named("GamePlayApplication")
                    .LifeStyle.HybridPerWebRequestPerThread()
                );
        }
    }
}
