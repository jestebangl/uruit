﻿namespace UruIT.GameOfDrones.Service.DependencyResolution.ApplicationInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using UruIT.GameOfDrones.Application.Validator.Validator;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;

    public class ValidatorInstaller : IWindsorInstaller
    {
        public ValidatorInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IGameValidator>().ImplementedBy<GameValidator>().Named("GameValidator")
                    .LifestylePerWebRequest());
        }

    }
}
