﻿namespace UruIT.GameOfDrones.Service.DependencyResolution.ServiceInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces;
    using UruIT.GameOfDrones.Service.Orchestrator.Service;

    public class OrchestratorInstaller : IWindsorInstaller
    {

        public OrchestratorInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IMovementService>().ImplementedBy<MovementService>().Named("MovementService")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IUserService>().ImplementedBy<UserService>().Named("UserService")
                    .LifeStyle.HybridPerWebRequestPerThread(),
                
                Component.For<IGamePlayService>().ImplementedBy<GamePlayService>().Named("GamePlayService")
                    .LifeStyle.HybridPerWebRequestPerThread()
                );
        }
    }
}
