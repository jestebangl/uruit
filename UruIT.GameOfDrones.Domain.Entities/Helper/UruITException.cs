﻿using System;
using System.Resources;
using System.Runtime.Serialization;
using UruIT.GameOfDrones.Domain.Entities.Enum;

[assembly: NeutralResourcesLanguage("es")]
namespace UruIT.GameOfDrones.Domain.Entities.Helper
{


    [Serializable]
    public class UruITException : Exception
    {
        public UruITException() : base() { }

        public UruITException(string message) : base(message)
        {
        }

        /// <summary>
        /// Thrown a new UruITException with a message for the user
        /// </summary>
        /// <param name="methodException">Current exception method</param>
        /// <param name="userExceptionMessage">Message to throw to the user</param>
        /// <param name="ex">Exception when ocurrs</param>
        public UruITException(string methodException, ErrorMessagesEnum userExceptionMessage, Exception ex = null)
        {
            if (ex != null)
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {ex.ToString()}");
            }
            else
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {userExceptionMessage.ToDescriptionString()}");
            }

            throw new UruITException(userExceptionMessage.ToDescriptionString());
        }

        protected UruITException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
