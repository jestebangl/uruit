﻿namespace UruIT.GameOfDrones.Domain.Entities.Models
{
    public class MovementModel
    {
        public long MovementId { get; set; }

        public string MovementName { get; set; }

        public MovementModel() { }
    }
}
