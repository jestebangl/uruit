﻿namespace UruIT.GameOfDrones.Domain.Entities.Models
{
    public class GamePlayDetailModel
    {
        public long GamePlayDetailId { get; set; }

        public int GameId { get; set; }

        public long GamePlayId { get; set; }

        public long UserOneId { get; set; }

        public long MovementUserOneId { get; set; }

        public long UserTwoId { get; set; }

        public long MovementUserTwoId { get; set; }

        public GamePlayDetailModel()
        {

        }
    }
}
