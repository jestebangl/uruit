﻿namespace UruIT.GameOfDrones.Domain.Entities.Models
{
    public class UserModel
    {
        public long UserId { get; set; }

        public string UserName { get; set; }

        public UserModel() { }

        public UserModel(long userId, string userName)
        {
            this.UserId = userId;
            this.UserName = userName;
        }
    }
}
