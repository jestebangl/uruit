﻿namespace UruIT.GameOfDrones.Domain.Entities.Models
{
    using System;

    public class GamePlayModel
    {
        public long GamePlayId { get; set; }

        public long UserOne { get; set; }

        public long UserOneWins { get; set; }

        public long UserTwo { get; set; }

        public long UserTwoWins { get; set; }

        public DateTime GameDate { get; set; }

        public GamePlayModel() { }
    }
}
