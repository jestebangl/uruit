﻿namespace UruIT.GameOfDrones.Domain.Entities.Models
{
    using System.Data;


    public class ModelDao
    {
        public string ParamName { get; set; }

        public SqlDbType SqlDbType { get; set; }

        public object Value { get; set; }

        public bool IsOut { get; set; }

        public object ValueOut { get; set; }

        public ModelDao(string paramName, SqlDbType sqlDbType, object value)
        {
            this.ParamName = paramName;
            this.SqlDbType = sqlDbType;
            this.Value = value;
        }
    }
}
