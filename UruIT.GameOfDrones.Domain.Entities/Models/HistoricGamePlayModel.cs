﻿namespace UruIT.GameOfDrones.Domain.Entities.Models
{
    using System;


    public class HistoricGamePlayModel
    {
        public long GamePlayId { get; set; }

        public string Player1 { get; set; }

        public int UserOneWins { get; set; }

        public string Player2 { get; set; }

        public int UserTwoWins { get; set; }

        public DateTime GameDate { get; set; }
    }
}
