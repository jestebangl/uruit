﻿using System.ComponentModel;

namespace UruIT.GameOfDrones.Domain.Entities.Enum
{
    public enum ErrorMessagesEnum
    {
        [Description("The movements list has not been found.")]
        MovementsNotFound = 1,
        [Description("The user information was not found, try again later.")]
        UserIdNorFound = 2,
        [Description("The game couldnt be starter, try again later.")]
        GamePlayIdNotFound = 3,
        [Description("Value can not be empty")]
        ValueCanNotBeNull = 4,
        [Description("Value can not be null")]
        ValueCanNotBeEmpty = 5,
        [Description("There isn´t historic records to show")]
        HistoricFilesNotFound = 6,
        [Description("Right now ther is a problem with our services, please try again later.")]
        UnexpectedError = 7,
    }

    public static partial class ErrorMessageEnumExtention
    {
        public static string ToDescriptionString(this ErrorMessagesEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
