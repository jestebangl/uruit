import { Component, OnInit } from '@angular/core';
import { ConectorApiHelper } from '../Core/Helper/conectroApiHelper';
import { environment } from 'src/environments/environment';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-game-record',
  templateUrl: './game-record.component.html',
  styleUrls: ['./game-record.component.css']
})
export class GameRecordComponent implements OnInit {

  public pageIndex = 1;
  public gameRecord: any[] = [];
  public loading = false;
  private toasterService: ToasterService;

  constructor(private conectorApiHelper: ConectorApiHelper,
    toasterService: ToasterService) {
      this.toasterService = toasterService;
    this.GetGameRecords();
  }

  ngOnInit() {
  }

  public GetGameRecords() {
    this.loading = true;
    const url = environment.GetHistoricGamePlay
      .replace('[rowCountPerPage]', environment.rowCountPerPage)
      .replace('[pageIndex]', this.pageIndex.toString());

    this.conectorApiHelper.ResolverPeticionGet(url).then(response => {
      this.loading = false;
      this.gameRecord = this.conectorApiHelper.ObtenerBodyRespuesta(response);
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    });

  }

  public SearchPrevious() {
    if (this.pageIndex > 1 ) {
      this.pageIndex = this.pageIndex - 1;
      this.GetGameRecords();
    }
  }

  public SearchNext() {
    this.pageIndex = this.pageIndex + 1;
    this.GetGameRecords();
  }

}
