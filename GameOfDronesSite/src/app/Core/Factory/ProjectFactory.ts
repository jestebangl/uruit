import { UserModel } from '../Models/UserModel';
import { ErrorMessageHelper } from '../Helper/errorMessageHelper';
import { GamePlayDetail } from '../Models/GamePlayDetail';
import { CssListModel } from '../Models/CssListModel';

export function getUserModelInstance() {
    return new UserModel();
}
export function getErrorMessageHelperInstance() {
    return new ErrorMessageHelper();
}

export function getGamePlayDetailInstance() {
    return new GamePlayDetail();
}

export function getCssListModelInstance(elementRef) {
    return new CssListModel(elementRef);
}

