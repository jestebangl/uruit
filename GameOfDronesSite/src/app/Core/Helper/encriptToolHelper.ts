import * as CryptoJS from '../../../../node_modules/crypto-js';
import { Base64 } from 'js-base64';

export class EncriptToolHelper {
  private key = CryptoJS.enc.Utf8.parse('TUFIFAQTUAAECDZD');
  private iv = CryptoJS.enc.Utf8.parse('!QAZxsw2#EDCvfr4');

    constructor () {
    }

    public $Encrypted(request): string {
        try {
          const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(request), this.key, {
            keySize: 256,
            iv: this.iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
          });

          return encrypted;
        } catch (error) {
          return error;
        }
      }

      public $Decrypted(dataFinal): string {
        const decrypted = CryptoJS.AES.decrypt(dataFinal, this.key, {
          keySize: 256,
          iv: this.iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
        });
        return Base64.decode(decrypted.toString(CryptoJS.enc.Utf8));
      }

      public $DecryptedNotBase64(dataFinal): string {
        const decrypted = CryptoJS.AES.decrypt(dataFinal, this.key, {
          keySize: 256,
          iv: this.iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
        });

        return decrypted.toString(CryptoJS.enc.Utf8);
      }
}
