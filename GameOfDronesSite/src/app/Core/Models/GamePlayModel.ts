export class GamePlayModel {
    public GamePlayId: number;
    public UserOne: number;
    public UserOneWins: number;
    public UserTwo: number;
    public UserTwoWins: number;
    public GameDate: number;

    constructor() { }

}
