export class GamePlayDetail {
    public GamePlayDetailId: number;
    public GameId: number;
    public GamePlayId: number;
    public UserOneId: number;
    public MovementUserOneId: number;
    public UserTwoId: number;
    public MovementUserTwoId: number;

    constructor () { }
}
