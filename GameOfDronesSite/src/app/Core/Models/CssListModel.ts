import { ElementRef } from '@angular/core';

export class CssListModel {
    public colorList = [
        {
            text: 'UruIT Green',
            value: '#1ed760'
        },
        {
            text: 'UruIT Dark',
            value: '#21282f'
        },
        {
            text: 'UruIT Default',
            value: '#FFF'
        },
        {
            text: 'UruIT Grey',
            value: '#lightgrey'
        }
    ];

    public fontList = [
        {
            text: 'Georgia',
            value: 'Georgia'
        },
        {
            text: 'Book Antiqua',
            value: 'Book Antiqua'
        },
        {
            text: 'Times New Roman',
            value: 'Times New Roman'
        },
        {
            text: 'Palatino Linotype',
            value: 'Palatino Linotype'
        },
        {
            text: 'Arial',
            value: 'Arial'
        },
        {
            text: 'Helvetica',
            value: 'Helvetica'
        },
        {
            text: 'Verdana',
            value: 'Verdana'
        },
        {
            text: 'Courier New',
            value: 'Courier New'
        },
        {
            text: 'Lucida Console',
            value: 'Lucida Console'
        }
    ];

    constructor(private elementRef: ElementRef) { }

    public ChangeColor(newColor) {
        this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = newColor;
    }

    public ChangeFont(fontName) {
        const htmlElements: any[] = [
            'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'span', 'input', 'label', 'div'
        ];

        this.ChangeFontByElementType(htmlElements, fontName);
    }

    public ChangeFontColor(color) {
        const htmlElements: any[] = [
            'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'span', 'input', 'label'
        ];

        this.ChangeFontColorByElementType(htmlElements, color);
    }

    private ChangeFontByElementType(elemenType: any[], fontName) {
        for (let i = 0; i < elemenType.length; i++) {
            const elems = document.getElementsByTagName(elemenType[i]);
            for (let j = 0; j < elems.length; j++) {
                elems[j].style.fontFamily = fontName;
            }
        }
    }

    private ChangeFontColorByElementType(elemenType: any[], color) {
        for (let i = 0; i < elemenType.length; i++) {
            const elems = document.getElementsByTagName(elemenType[i]);
            for (let j = 0; j < elems.length; j++) {
                elems[j].style.color = color;
            }
        }
    }
}
