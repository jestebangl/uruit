import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app-routing';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NgxLoadingModule } from 'ngx-loading';
import { GameStartComponent } from './game-start/game-start.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConectorApiHelper } from './Core/Helper/conectroApiHelper';
import { HttpModule } from '@angular/http';
import { ExecutingGameComponent } from './executing-game/executing-game.component';
import { GameRecordComponent } from './game-record/game-record.component';
import { ToasterModule, ToasterService } from 'angular2-toaster';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    GameStartComponent,
    ExecutingGameComponent,
    GameRecordComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    ToasterModule.forRoot()

  ],
  providers: [appRoutingProviders, ConectorApiHelper],
  bootstrap: [AppComponent]
})
export class AppModule { }
