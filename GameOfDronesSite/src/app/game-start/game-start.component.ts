import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '../Core/Models/UserModel';
import { getUserModelInstance, getErrorMessageHelperInstance,  } from '../Core/Factory/ProjectFactory';
import { ErrorMessageHelper } from '../Core/Helper/errorMessageHelper';
import { ConectorApiHelper } from '../Core/Helper/conectroApiHelper';
import { environment } from 'src/environments/environment';
import { GamePlayModel } from '../Core/Models/GamePlayModel';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-game-start',
  templateUrl: './game-start.component.html',
  styleUrls: ['./game-start.component.css']
})
export class GameStartComponent implements OnInit {

  @Output() gameStartedEmit: EventEmitter<any> = new EventEmitter<any>();

  public gameStartForm: FormGroup;
  public playerOne: UserModel;
  public playerTwo: UserModel;
  public errorMessageHelper: ErrorMessageHelper;
  public currentGamePlayId: number;
  public gamePlayModel: GamePlayModel;
  public loading = false;
  private toasterService: ToasterService;


  constructor(private conectorApiHelper: ConectorApiHelper,
    toasterService: ToasterService) {
    this.toasterService = toasterService;
    this.playerOne = getUserModelInstance();
    this.playerTwo = getUserModelInstance();
    this.errorMessageHelper = getErrorMessageHelperInstance();

  }

  ngOnInit() {
    this.InitialiceFormGroup();
  }

  public StartGame() {
    this.GetUserInfo();
  }

  private EmmitGameStart() {
    const gameStart: any = {
      playerOne: this.playerOne,
      playerTwo: this.playerTwo,
      GamePlayModel: this.gamePlayModel
    };

    this.gameStartedEmit.emit(gameStart);
  }

  private AddNewGamePlay() {
    this.loading = true;
    const userModels: UserModel[] = [];
    userModels.push(this.playerOne);
    userModels.push(this.playerTwo);

    this.conectorApiHelper.ResolverPeticionPost(environment.AddNewGamePlay, userModels).then(response => {
      this.loading = false;

      const body = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      this.gamePlayModel = body;
      this.EmmitGameStart();
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuesta(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    });
  }

  private GetUserInfo() {
    this.loading = true;
    this.conectorApiHelper.ResolverPeticionPost(environment.InsertAndGetUser, this.playerOne).then(response => {
      const body = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      this.playerOne.UserId = body.UserId;

      this.conectorApiHelper.ResolverPeticionPost(environment.InsertAndGetUser, this.playerTwo).then(responseTwo => {
        const bodyTwo = this.conectorApiHelper.ObtenerBodyRespuesta(responseTwo);
        this.playerTwo.UserId = bodyTwo.UserId;

        this.AddNewGamePlay();
      }).catch(error => {
        this.loading = false;
        const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
        this.toasterService.pop('error', 'Error', errorMessagge);
      });
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    });
  }

  private InitialiceFormGroup() {
    this.gameStartForm = new FormGroup({
      playerOneUserName: new FormControl(
        this.playerOne.UserName,
        [
          Validators.required,
          Validators.maxLength(50)
        ]
      ),
      playerTwoUserName: new FormControl(
        this.playerTwo.UserName,
        [
          Validators.required,
          Validators.maxLength(50)
        ]
      )
    });
  }


  public ValidateCSSControl(control) {
    return {
      'is-valid': !this.gameStartForm.controls[control].errors,
      'is-invalid': this.gameStartForm.controls[control].errors,
      'form-control': true
    };
  }

  public ValidateErrorControl(control) {
    return (
      this.gameStartForm.controls[control].errors
    );
  }

  public ShowErrorMessage(control) {
    return {
      'invalid-feedback': this.gameStartForm.controls[control].errors
    };
  }

  public DisableSubmitButton() {
    return this.gameStartForm.invalid;
  }
}
