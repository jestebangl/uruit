import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components Import
import { MainComponent } from './main/main.component';
import { AppComponent } from './app.component';
import { GameRecordComponent } from './game-record/game-record.component';

const appRoutes: Routes = [
    {path: '', component: MainComponent},
    {path: 'GameRecords', component: GameRecordComponent},

];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
