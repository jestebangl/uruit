import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConectorApiHelper } from '../Core/Helper/conectroApiHelper';
import { environment } from 'src/environments/environment';
import { ErrorMessageHelper } from '../Core/Helper/errorMessageHelper';
import { getErrorMessageHelperInstance, getGamePlayDetailInstance } from '../Core/Factory/ProjectFactory';
import { GamePlayDetail } from '../Core/Models/GamePlayDetail';
import { UserModel } from '../Core/Models/UserModel';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-executing-game',
  templateUrl: './executing-game.component.html',
  styleUrls: ['./executing-game.component.css']
})
export class ExecutingGameComponent implements OnInit {

  @Input() gamePlayInformation: any;
  @Output() gameWinnerEmitter: EventEmitter<any> = new EventEmitter<any>();


  public errorMessageHelper: ErrorMessageHelper;
  public gameExecutionForm: FormGroup;
  public movementList: any = [];
  public movementSelected = 0;
  public currentNumberMatch = 1;
  public currenGamePlayDetail: GamePlayDetail;
  public currentPlayer: UserModel;

  public playerOneHasPlayed = false;
  public playerTwoHasPlayed = false;

  public matchHistory: any[] = [];
  public loading = false;
  private toasterService: ToasterService;


  constructor(private conectorApiHelper: ConectorApiHelper,
    toasterService: ToasterService) {
    this.toasterService = toasterService;
    this.InitialiceFormGroup();
    this.GetMovementList();

    this.errorMessageHelper = getErrorMessageHelperInstance();
    this.currenGamePlayDetail = getGamePlayDetailInstance();


  }

  ngOnInit() {
  }

  public NextStep() {
    this.ValidateNextStep();
  }

  private ValidateNextStep() {
    if (!this.playerOneHasPlayed && !this.playerTwoHasPlayed) {
      this.playerOneHasPlayed = true;
      this.currenGamePlayDetail.MovementUserOneId = this.movementSelected;
      this.currentPlayer = this.gamePlayInformation.playerTwo;
      this.movementSelected = 0;
     } else if (this.playerOneHasPlayed && !this.playerTwoHasPlayed) {
      this.currenGamePlayDetail.MovementUserTwoId = this.movementSelected;
      this.SaveCurrentMatch();
    }
  }

  private SaveCurrentMatch() {
    this.loading = true;
    this.conectorApiHelper.ResolverPeticionPost(environment.ProcessGameMovement, this.currenGamePlayDetail).then(response => {
      const winnerUserId = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      const currentMatchWinner = {
        UserName: winnerUserId === 0 ? 'Empate' : this.GetUserNameWinner(winnerUserId),
        Round: this.currentNumberMatch
      };

      this.matchHistory.push(currentMatchWinner);
      this.StartNewMatch();
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    });
  }

  private GetUserNameWinner(winnerUserId) {
    if (this.gamePlayInformation.playerOne.UserId === winnerUserId) {
      return this.gamePlayInformation.playerOne.UserName;
    } else {
      return this.gamePlayInformation.playerTwo.UserName;
    }
  }

  public StartNewMatch() {
    this.playerOneHasPlayed = false;
    this.playerTwoHasPlayed = false;
    this.movementSelected = 0;
    this.currentNumberMatch ++;
    this.HasAPlayerWon();
  }

  public SetGamePlayInfomation(gamePlayInformation) {
    this.gamePlayInformation = gamePlayInformation;
    this.currentPlayer = this.gamePlayInformation.playerOne;
    this.SetGamePlayDetailInfomation();

  }

  private HasAPlayerWon() {
    this.conectorApiHelper.ResolverPeticionPost(environment.HasAPlayerWon, this.currenGamePlayDetail).then(response => {
      this.loading = false;
      const alreadyWon = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      if (alreadyWon) {
        this.currentNumberMatch = 0;
        this.currentPlayer = undefined;
        this.gameWinnerEmitter.emit(this.matchHistory[this.matchHistory.length - 1]);
      } else {
        this.currentPlayer = this.gamePlayInformation.playerOne;
        this.SetGamePlayDetailInfomation();
      }
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    });
  }

  private SetGamePlayDetailInfomation() {
    this.currenGamePlayDetail = getGamePlayDetailInstance();
    this.currenGamePlayDetail.GameId = this.currentNumberMatch;
    this.currenGamePlayDetail.GamePlayId = this.gamePlayInformation.GamePlayModel.GamePlayId;
    this.currenGamePlayDetail.UserOneId = this.gamePlayInformation.playerOne.UserId;
    this.currenGamePlayDetail.UserTwoId = this.gamePlayInformation.playerTwo.UserId;
  }

  private GetMovementList() {
    this.loading = true;
    this.conectorApiHelper.ResolverPeticionGet(environment.GetMovementList).then(response => {
      this.loading = false;

      this.movementList = this.conectorApiHelper.ObtenerBodyRespuesta(response);
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    });
  }

  private InitialiceFormGroup() {
    this.gameExecutionForm = new FormGroup({
      movementSelected: new FormControl(
        this.movementSelected,
        [
          Validators.required,
          Validators.min(1)
        ]
      ),
    });
  }

  public ValidateCSSControl(control) {
    return {
      'is-valid': this.gameExecutionForm !== undefined && !this.gameExecutionForm.controls[control].errors,
      'is-invalid': this.gameExecutionForm !== undefined && this.gameExecutionForm.controls[control].errors,
      'form-control': true
    };
  }

  public ValidateErrorControl(control) {
    return (
      this.gameExecutionForm !== undefined && this.gameExecutionForm.controls[control].errors
    );
  }

  public ShowErrorMessage(control) {
    return {
      'invalid-feedback': this.gameExecutionForm !== undefined && this.gameExecutionForm.controls[control].errors
    };
  }

  public DisableSubmitButton() {
    return this.gameExecutionForm.invalid;
  }

}
