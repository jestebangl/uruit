import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutingGameComponent } from './executing-game.component';

describe('ExecutingGameComponent', () => {
  let component: ExecutingGameComponent;
  let fixture: ComponentFixture<ExecutingGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutingGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutingGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
