import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { GameStartComponent } from '../game-start/game-start.component';
import { ExecutingGameComponent } from '../executing-game/executing-game.component';
import { CssListModel } from '../Core/Models/CssListModel';
import { getCssListModelInstance } from '../Core/Factory/ProjectFactory';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements AfterViewInit {

  @ViewChild('gameStartComponent') gameStartComponent: GameStartComponent;
  @ViewChild('executingGameComponent') executingGameComponent: ExecutingGameComponent;

  public hasGameStarted: Boolean = false;
  public gamePlayInformation: any;
  public hasGameOver: Boolean = false;
  public gameWinnerUser: any;
  public cssListModel: CssListModel;

  constructor(private elementRef: ElementRef) {
    this.cssListModel = getCssListModelInstance(elementRef);
  }

  ngAfterViewInit() {
    this.gameStartComponent.gameStartedEmit
      .subscribe(
        res => {
          this.hasGameStarted = true;
          this.gamePlayInformation = res;
          this.executingGameComponent.SetGamePlayInfomation(res);
        }
      );

    this.executingGameComponent.gameWinnerEmitter
    .subscribe(
      res => {
        this.hasGameOver = true;
        this.gameWinnerUser = res;
      }
    );

  }

  public PlayAgain() {
    window.location.href = window.location.href;
  }

}
