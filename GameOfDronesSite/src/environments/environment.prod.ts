declare var data: any;

export const environment = {
  production: true,
  rowCountPerPage: '5',
  InsertAndGetUser: data.basePathWebApi + 'api/user/insertandgetuser',
  GetMovementList: data.basePathWebApi + 'api/movement/getmovementlist',
  AddNewGamePlay: data.basePathWebApi + 'api/gameplay/addnewgameplay/',
  GetNumerOfMatches: data.basePathWebApi + 'api/configuration/getnumerofmatches',
  ProcessGameMovement: data.basePathWebApi + 'api/gameplay/processgamemovement/',
  HasAPlayerWon: data.basePathWebApi + 'api/gameplay/hasaplayerwon/',
  GetHistoricGamePlay: data.basePathWebApi + 'api/gameplay/gethistoricgameplay/?rowCountPerPage=[rowCountPerPage]&pageIndex=[pageIndex]'
};
