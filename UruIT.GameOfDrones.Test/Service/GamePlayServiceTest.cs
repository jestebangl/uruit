﻿using Castle.Windsor;
using NUnit.Framework;
using UruIT.GameOfDrones.Domain.Entities.Models;
using UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces;
using UruIT.GameOfDrones.Service.DependencyResolution.ApplicationInstaller;
using UruIT.GameOfDrones.Service.DependencyResolution.InfraestructureInstaller;
using UruIT.GameOfDrones.Service.DependencyResolution.ServiceInstaller;

namespace UruIT.GameOfDrones.Test.Service
{
    [TestFixture]
    public class GamePlayServiceTest
    {
        private IWindsorContainer Container;

        [SetUp]
        public void SetUp()
        {
            var windsor = Ragolo.Core.IoC.IocHelper.Instance;
            Container = windsor.GetContainer();

            string ConnectionString = @"Server=JE-PC\SQLEXPRESS; Database=UruITGameOfDrones; Integrated Security=True;";
            Container = new WindsorContainer();
            Container.Install(new DataAccessInstaller(ConnectionString));

            Container.Install(new OperatorInstaller());
            Container.Install(new ValidatorInstaller());

            Container.Install(new OrchestratorInstaller());

        }

        [Test]
        public void When_PlayerOneMovement_Equals_PlayerTwoMovement()
        {
            IGamePlayService GamePlayService = Container.Resolve<IGamePlayService>();

            GamePlayDetailModel gamePlayModel = new GamePlayDetailModel()
            {
                GameId = 2,
                GamePlayId = 32,
                MovementUserOneId = 1,
                MovementUserTwoId = 1,
                UserOneId = 3,
                UserTwoId = 4
            };

            var response = GamePlayService.ProcessGameMovement(gamePlayModel);
            Assert.AreEqual(0, response);

        }

        [Test]
        public void When_Scissors_Paper_Winner()
        {
            IGamePlayService GamePlayService = Container.Resolve<IGamePlayService>();

            GamePlayDetailModel gamePlayModel = new GamePlayDetailModel()
            {
                GameId = 2,
                GamePlayId = 32,
                MovementUserOneId = 1,
                MovementUserTwoId = 3,
                UserOneId = 3,
                UserTwoId = 4
            };

            var response = GamePlayService.ProcessGameMovement(gamePlayModel);
            Assert.AreEqual(gamePlayModel.UserOneId, response);

        }


        [Test]
        public void When_Scissors_Rock_Winner()
        {
            IGamePlayService GamePlayService = Container.Resolve<IGamePlayService>();

            GamePlayDetailModel gamePlayModel = new GamePlayDetailModel()
            {
                GameId = 2,
                GamePlayId = 32,
                MovementUserOneId = 3,
                MovementUserTwoId = 2,
                UserOneId = 3,
                UserTwoId = 4
            };

            var response = GamePlayService.ProcessGameMovement(gamePlayModel);
            Assert.AreEqual(gamePlayModel.UserTwoId, response);

        }

        [Test]
        public void When_Paper_Rock_Winner()
        {
            IGamePlayService GamePlayService = Container.Resolve<IGamePlayService>();

            GamePlayDetailModel gamePlayModel = new GamePlayDetailModel()
            {
                GameId = 2,
                GamePlayId = 32,
                MovementUserOneId = 1,
                MovementUserTwoId = 2,
                UserOneId = 3,
                UserTwoId = 4
            };

            var response = GamePlayService.ProcessGameMovement(gamePlayModel);
            Assert.AreEqual(gamePlayModel.UserOneId, response);

        }


        [TearDown]
        public void Cleanup()
        {
            Container.Dispose();
        }
    }
}
