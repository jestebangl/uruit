﻿namespace UruIT.GameOfDrones.Infraestructure.DataAccess.Dao
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using UruIT.GameOfDrones.Domain.Entities.Logger;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces;

    public class DataAccessObject : IDataAccessObject
    {
        public string ConnectionString { get; set; }

        public DataAccessObject(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
        }

        
        public DataSet SqlQueryResult(string sqlProcedueName,  params ModelDao[] parameters)
        {
            try
            {
                DataSet resultDataSet = new DataSet();

                using (SqlConnection cnn = new SqlConnection(ConnectionString))
                using (SqlCommand cmd = new SqlCommand())
                {
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = sqlProcedueName;

                    SqlParameter param;
                    foreach (var p in parameters)
                    {
                        param = cmd.Parameters.Add("@" + p.ParamName, p.SqlDbType);
                        if (p.IsOut)
                        {
                            param.Direction = ParameterDirection.Output;
                        }
                        else
                        {
                            param.Value = p.Value;
                        }
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(resultDataSet);
                        cnn.Close();
                    }

                    return resultDataSet;

                }
            }
            catch (SqlException se)
            {
                Logger.Current.Error($"Sql UpdateError: {se.ToString()}");
                throw se;
            }
            catch (Exception e)
            {
                Logger.Current.Error($"Sql UpdateError: {e.ToString()}");
                throw e;
            }
        }

        
        public bool SqlUpdate(string sqlProcedueName, params ModelDao[] parameters)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(ConnectionString))
                using (SqlCommand cmd = new SqlCommand())
                {
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = sqlProcedueName;

                    SqlParameter param;
                    foreach (var p in parameters)
                    {
                        param = cmd.Parameters.Add("@" + p.ParamName, p.SqlDbType);
                        if (p.IsOut)
                        {
                            param.Direction = ParameterDirection.Output;
                        }
                        else
                        {
                            param.Value = p.Value;
                        }
                    }

                    int result = cmd.ExecuteNonQuery();
                    cnn.Close();

                    return result > 0 ? true : false;

                }
            }
            catch (SqlException se)
            {
                Logger.Current.Error($"Sql UpdateError: {se.ToString()}");
                throw se;
            }
            catch (Exception e)
            {
                Logger.Current.Error($"Sql UpdateError: {e.ToString()}");
                throw e;
            }
        }
    }
}
