IF NOT EXISTS (SELECT * FROM sys.objects WHERE name Like 'GameDronesRules')
BEGIN
	CREATE TABLE GameDronesRules(
		WinAgainsToMovementId BIGINT,
		LoseAgainstToMovementId BIGINT,
		Keyword VARCHAR(20),
		PRIMARY KEY (WinAgainsToMovementId, LoseAgainstToMovementId)
	);
END
GO

