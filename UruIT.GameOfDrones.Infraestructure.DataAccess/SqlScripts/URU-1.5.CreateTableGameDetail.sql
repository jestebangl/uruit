IF NOT EXISTS (SELECT * FROM sys.objects WHERE name Like 'GamePlayDetail')
BEGIN
	CREATE TABLE GamePlayDetail(
		GamePlayDetailId BIGINT IDENTITY PRIMARY KEY,
		GameId INT,
		GamePlayId BIGINT,
		UserOneId BIGINT,
		MovementUserOneId BIGINT,
		UserTwoId BIGINT,
		MovementUserTwoId BIGINT,
		CONSTRAINT FK_UserOneId_User FOREIGN KEY (UserOneId) REFERENCES dbo.[User](IdUser),
		CONSTRAINT FK_UserTwoId_User FOREIGN KEY (UserTwoId) REFERENCES dbo.[User](IdUser),
		CONSTRAINT FK_MovementUserOneId_Movement FOREIGN KEY (MovementUserOneId) REFERENCES dbo.Movement(MovementId),
		CONSTRAINT FK_MovementUserTwoId_Movement FOREIGN KEY (MovementUserTwoId) REFERENCES dbo.Movement(MovementId),
		CONSTRAINT FK_GamePlayId_GamePlay FOREIGN KEY (GamePlayId) REFERENCES dbo.GamePlay(GamePlayId)
	);
END
GO

