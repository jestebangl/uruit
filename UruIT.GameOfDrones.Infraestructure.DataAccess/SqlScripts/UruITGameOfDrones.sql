USE [master]
GO
/****** Object:  Database [UruITGameOfDrones]    Script Date: 3/03/2019 11:22:22 p. m. ******/
CREATE DATABASE [UruITGameOfDrones]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UruITGameOfDrones', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\UruITGameOfDrones.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'UruITGameOfDrones_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\UruITGameOfDrones_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [UruITGameOfDrones] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UruITGameOfDrones].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UruITGameOfDrones] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET ARITHABORT OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UruITGameOfDrones] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UruITGameOfDrones] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UruITGameOfDrones] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UruITGameOfDrones] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UruITGameOfDrones] SET  MULTI_USER 
GO
ALTER DATABASE [UruITGameOfDrones] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UruITGameOfDrones] SET DB_CHAINING OFF 
GO
ALTER DATABASE [UruITGameOfDrones] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [UruITGameOfDrones] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [UruITGameOfDrones] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [UruITGameOfDrones] SET QUERY_STORE = OFF
GO
USE [UruITGameOfDrones]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [UruITGameOfDrones]
GO
/****** Object:  Table [dbo].[GameDronesRules]    Script Date: 3/03/2019 11:22:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameDronesRules](
	[WinAgainsToMovementId] [bigint] NOT NULL,
	[LoseAgainstToMovementId] [bigint] NOT NULL,
	[Keyword] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[WinAgainsToMovementId] ASC,
	[LoseAgainstToMovementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GamePlay]    Script Date: 3/03/2019 11:22:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GamePlay](
	[GamePlayId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserOne] [bigint] NULL,
	[UserOneWins] [bigint] NULL,
	[UserTwo] [bigint] NULL,
	[UserTwoWins] [bigint] NULL,
	[GameDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[GamePlayId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GamePlayDetail]    Script Date: 3/03/2019 11:22:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GamePlayDetail](
	[GamePlayDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[GameId] [int] NULL,
	[GamePlayId] [bigint] NULL,
	[UserOneId] [bigint] NULL,
	[MovementUserOneId] [bigint] NULL,
	[UserTwoId] [bigint] NULL,
	[MovementUserTwoId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[GamePlayDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movement]    Script Date: 3/03/2019 11:22:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movement](
	[MovementId] [bigint] NOT NULL,
	[MovementName] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[MovementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 3/03/2019 11:22:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[IdUser] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GameDronesRules] ([WinAgainsToMovementId], [LoseAgainstToMovementId], [Keyword]) VALUES (1, 2, N'BEATS')
INSERT [dbo].[GameDronesRules] ([WinAgainsToMovementId], [LoseAgainstToMovementId], [Keyword]) VALUES (2, 3, N'BEATS')
INSERT [dbo].[GameDronesRules] ([WinAgainsToMovementId], [LoseAgainstToMovementId], [Keyword]) VALUES (3, 1, N'BEATS')
INSERT [dbo].[Movement] ([MovementId], [MovementName]) VALUES (1, N'PAPER')
INSERT [dbo].[Movement] ([MovementId], [MovementName]) VALUES (2, N'ROCK')
INSERT [dbo].[Movement] ([MovementId], [MovementName]) VALUES (3, N'SCISSORS')
ALTER TABLE [dbo].[GamePlay]  WITH CHECK ADD  CONSTRAINT [FK_UserOne_User] FOREIGN KEY([UserOne])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[GamePlay] CHECK CONSTRAINT [FK_UserOne_User]
GO
ALTER TABLE [dbo].[GamePlay]  WITH CHECK ADD  CONSTRAINT [FK_UserTwo_User] FOREIGN KEY([UserTwo])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[GamePlay] CHECK CONSTRAINT [FK_UserTwo_User]
GO
ALTER TABLE [dbo].[GamePlayDetail]  WITH CHECK ADD  CONSTRAINT [FK_GamePlayId_GamePlay] FOREIGN KEY([GamePlayId])
REFERENCES [dbo].[GamePlay] ([GamePlayId])
GO
ALTER TABLE [dbo].[GamePlayDetail] CHECK CONSTRAINT [FK_GamePlayId_GamePlay]
GO
ALTER TABLE [dbo].[GamePlayDetail]  WITH CHECK ADD  CONSTRAINT [FK_MovementUserOneId_Movement] FOREIGN KEY([MovementUserOneId])
REFERENCES [dbo].[Movement] ([MovementId])
GO
ALTER TABLE [dbo].[GamePlayDetail] CHECK CONSTRAINT [FK_MovementUserOneId_Movement]
GO
ALTER TABLE [dbo].[GamePlayDetail]  WITH CHECK ADD  CONSTRAINT [FK_MovementUserTwoId_Movement] FOREIGN KEY([MovementUserTwoId])
REFERENCES [dbo].[Movement] ([MovementId])
GO
ALTER TABLE [dbo].[GamePlayDetail] CHECK CONSTRAINT [FK_MovementUserTwoId_Movement]
GO
ALTER TABLE [dbo].[GamePlayDetail]  WITH CHECK ADD  CONSTRAINT [FK_UserOneId_User] FOREIGN KEY([UserOneId])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[GamePlayDetail] CHECK CONSTRAINT [FK_UserOneId_User]
GO
ALTER TABLE [dbo].[GamePlayDetail]  WITH CHECK ADD  CONSTRAINT [FK_UserTwoId_User] FOREIGN KEY([UserTwoId])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[GamePlayDetail] CHECK CONSTRAINT [FK_UserTwoId_User]
GO
/****** Object:  StoredProcedure [dbo].[AddNewGamePlatDetail]    Script Date: 3/03/2019 11:22:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddNewGamePlatDetail] @GameId INT,
											 @GamePlayId BIGINT,
											 @UserOneId BIGINT,
											 @MovementUserOneId BIGINT,
											 @UserTwoId BIGINT,
											 @MovementUserTwoId BIGINT
AS
BEGIN
	INSERT INTO [dbo].[GamePlayDetail]
           ([GameId]
           ,[GamePlayId]
           ,[UserOneId]
           ,[MovementUserOneId]
           ,[UserTwoId]
           ,[MovementUserTwoId])
     VALUES
           (@GameId
           ,@GamePlayId
           ,@UserOneId
           ,@MovementUserOneId
           ,@UserTwoId
           ,@MovementUserTwoId)
END
GO
/****** Object:  StoredProcedure [dbo].[AddNewGamePlayDetail]    Script Date: 3/03/2019 11:22:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddNewGamePlayDetail] @GameId INT,
											 @GamePlayId BIGINT,
											 @UserOneId BIGINT,
											 @MovementUserOneId BIGINT,
											 @UserTwoId BIGINT,
											 @MovementUserTwoId BIGINT
AS
BEGIN
	INSERT INTO [dbo].[GamePlayDetail]
           ([GameId]
           ,[GamePlayId]
           ,[UserOneId]
           ,[MovementUserOneId]
           ,[UserTwoId]
           ,[MovementUserTwoId])
     VALUES
           (@GameId
           ,@GamePlayId
           ,@UserOneId
           ,@MovementUserOneId
           ,@UserTwoId
           ,@MovementUserTwoId)
END
GO
/****** Object:  StoredProcedure [dbo].[GetHistoricMatches]    Script Date: 3/03/2019 11:22:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHistoricMatches] @RowCountPerPage INT,
   									  @PageIndex INT
AS
BEGIN
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
    SET DATEFORMAT DMY;

	WITH TempResult AS(

		SELECT GamePlayId ,
			u1.UserName AS Player1,
			[UserOneWins],
			u2.UserName AS Player2,
			[UserTwoWins],
			[GameDate]
			FROM [dbo].[GamePlay] gp
		INNER JOIN [dbo].[User] AS u1 ON (gp.[UserOne] = u1.[IdUser])
		INNER JOIN [dbo].[User] AS u2 ON (gp.[UserTwo] = u2.[IdUser])
	), TempCount AS (SELECT COUNT(*) AS TotalCount FROM TempResult)

	SELECT * FROM TempResult, TempCount
		  ORDER BY TempResult.[GameDate] DESC,
                TempResult.GamePlayId DESC
        OFFSET(@PageIndex - 1) * @RowCountPerPage ROWS FETCH NEXT @RowCountPerPage ROWS ONLY;	
END
GO
/****** Object:  StoredProcedure [dbo].[GetMovementList]    Script Date: 3/03/2019 11:22:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetMovementList]
AS
BEGIN
	SELECT *
	FROM [dbo].[Movement];
END
GO
/****** Object:  StoredProcedure [dbo].[HasAPlayerWon]    Script Date: 3/03/2019 11:22:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HasAPlayerWon] @RowCountPerPage INT,
   									  @PageIndex INT
AS
BEGIN
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
    SET DATEFORMAT DMY;

	WITH TempResult AS(

		SELECT GamePlayId ,
			u1.UserName AS Player1,
			[UserOneWins],
			u2.UserName AS Player2,
			[UserTwoWins],
			[GameDate]
			FROM [dbo].[GamePlay] gp
		INNER JOIN [dbo].[User] AS u1 ON (gp.[UserOne] = u1.[IdUser])
		INNER JOIN [dbo].[User] AS u2 ON (gp.[UserTwo] = u2.[IdUser])
	), TempCount AS (SELECT COUNT(*) AS TotalCount FROM TempResult)

	SELECT * FROM TempResult, TempCount
		  ORDER BY TempResult.[GameDate] DESC,
                TempResult.GamePlayId DESC
        OFFSET(@PageIndex - 1) * @RowCountPerPage ROWS FETCH NEXT @RowCountPerPage ROWS ONLY;	
END
GO
/****** Object:  StoredProcedure [dbo].[InsertAndGetUserId]    Script Date: 3/03/2019 11:22:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertAndGetUserId] @userName VARCHAR(50) 
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [User] WHERE UserName LIKE UPPER(@userName))
	BEGIN
		INSERT INTO [User](UserName) VALUES(UPPER(@userName));
	END

	SELECT IdUser FROM [User] WHERE UserName LIKE UPPER(@userName);
END
GO
/****** Object:  StoredProcedure [dbo].[InsertGamePlay]    Script Date: 3/03/2019 11:22:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertGamePlay] @IdUserOne BIGINT,
									   @IdUserTwo BIGINT
AS
BEGIN
	INSERT INTO GamePlay(GameDate, [UserOne], [UserOneWins], [UserTwo], [UserTwoWins])
	VALUES((SELECT GETDATE()), @IdUserOne, 0, @IdUserTwo, 0);

	SELECT * 
	FROM GamePlay
	WHERE GamePlayId = SCOPE_IDENTITY();
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateGamePlay]    Script Date: 3/03/2019 11:22:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateGamePlay] @IdUserWinner BIGINT,
									   @GamePlayId BIGINT
AS
BEGIN
	IF EXISTS(SELECT * FROM [dbo].[GamePlay] WHERE [GamePlayId]=@GamePlayId AND @IdUserWinner=[UserOne])
	BEGIN
		UPDATE [dbo].[GamePlay]
		SET [UserOneWins] = [UserOneWins] + 1
		WHERE [GamePlayId] = @GamePlayId;
	END
	ELSE IF EXISTS(SELECT * FROM [dbo].[GamePlay] WHERE [GamePlayId]=@GamePlayId AND @IdUserWinner=[UserOne])
	BEGIN
		UPDATE [dbo].[GamePlay]
		SET [UserTwoWins] = [UserTwoWins] + 1
		WHERE [GamePlayId] = @GamePlayId;
	END
	ELSE 
	BEGIN
		UPDATE [dbo].[GamePlay]
		SET [UserOneWins] = [UserOneWins] + 1,
			[UserTwoWins] = [UserTwoWins] + 1
		WHERE [GamePlayId] = @GamePlayId;
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ValidateGameDroneRules]    Script Date: 3/03/2019 11:22:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ValidateGameDroneRules] @MovementUserOneId BIGINT,
												@MovementUserTwoId BIGINT
AS
BEGIN
	IF EXISTS(SELECT * FROM [dbo].[GameDronesRules] WHERE [WinAgainsToMovementId]=@MovementUserOneId AND [LoseAgainstToMovementId]=@MovementUserTwoId)
	BEGIN 
		SELECT @MovementUserOneId AS Winner,
			[Keyword] AS KeyWord
		FROM [GameDronesRules]
		WHERE [WinAgainsToMovementId]=@MovementUserOneId AND 
		[LoseAgainstToMovementId]=@MovementUserTwoId;
	END
	ELSE IF EXISTS(SELECT * FROM [dbo].[GameDronesRules] WHERE [WinAgainsToMovementId]=@MovementUserTwoId AND [LoseAgainstToMovementId]=@MovementUserOneId)
	BEGIN
		SELECT @MovementUserTwoId AS Winner,
			[Keyword] AS KeyWord
		FROM [GameDronesRules]
		WHERE [WinAgainsToMovementId]=@MovementUserTwoId AND 
		[LoseAgainstToMovementId]=@MovementUserOneId
	END
END
GO
USE [master]
GO
ALTER DATABASE [UruITGameOfDrones] SET  READ_WRITE 
GO
