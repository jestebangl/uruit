IF NOT EXISTS (SELECT * FROM sys.objects WHERE name Like 'Movement')
BEGIN
	CREATE TABLE Movement(
		MovementId BIGINT PRIMARY KEY,
		MovementName VARCHAR(MAX)
	);
END
GO

