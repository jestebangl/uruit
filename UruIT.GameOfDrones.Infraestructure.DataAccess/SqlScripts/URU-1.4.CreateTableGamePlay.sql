IF NOT EXISTS (SELECT * FROM sys.objects WHERE name Like 'GamePlay')
BEGIN
	CREATE TABLE GamePlay(
		GamePlayId BIGINT IDENTITY PRIMARY KEY,
		UserOne BIGINT,
		UserOneWins BIGINT,
		UserTwo BIGINT,
		UserTwoWins BIGINT,
		GameDate DATETIME,
	    CONSTRAINT FK_UserOne_User FOREIGN KEY (UserOne) REFERENCES dbo.[User](IdUser),
		CONSTRAINT FK_UserTwo_User FOREIGN KEY (UserTwo) REFERENCES dbo.[User](IdUser)
	);
END
GO

