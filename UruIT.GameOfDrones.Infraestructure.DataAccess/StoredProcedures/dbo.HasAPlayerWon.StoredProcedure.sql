SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HasAPlayerWon]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[HasAPlayerWon] AS' 
END
GO
ALTER PROCEDURE [dbo].[HasAPlayerWon] @GamePlayId BIGINT
AS
BEGIN
	SELECT *
	FROM [dbo].[GamePlay]
	WHERE [GamePlayId] = @GamePlayId;
END
GO

