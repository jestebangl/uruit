SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMovementList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetMovementList] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetMovementList]
AS
BEGIN
	SELECT *
	FROM [dbo].[Movement];
END
GO

