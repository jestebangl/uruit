SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertGamePlay]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InsertGamePlay] AS' 
END
GO
ALTER PROCEDURE [dbo].[InsertGamePlay] @IdUserOne BIGINT,
									   @IdUserTwo BIGINT
AS
BEGIN
	INSERT INTO GamePlay(GameDate, [UserOne], [UserOneWins], [UserTwo], [UserTwoWins])
	VALUES((SELECT GETDATE()), @IdUserOne, 0, @IdUserTwo, 0);

	SELECT * 
	FROM GamePlay
	WHERE GamePlayId = SCOPE_IDENTITY();
END
GO



