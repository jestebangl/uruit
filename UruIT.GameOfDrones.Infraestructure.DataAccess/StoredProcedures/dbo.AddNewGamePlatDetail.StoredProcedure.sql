SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddNewGamePlayDetail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AddNewGamePlayDetail] AS' 
END
GO
ALTER PROCEDURE [dbo].[AddNewGamePlayDetail] @GameId INT,
											 @GamePlayId BIGINT,
											 @UserOneId BIGINT,
											 @MovementUserOneId BIGINT,
											 @UserTwoId BIGINT,
											 @MovementUserTwoId BIGINT
AS
BEGIN
	INSERT INTO [dbo].[GamePlayDetail]
           ([GameId]
           ,[GamePlayId]
           ,[UserOneId]
           ,[MovementUserOneId]
           ,[UserTwoId]
           ,[MovementUserTwoId])
     VALUES
           (@GameId
           ,@GamePlayId
           ,@UserOneId
           ,@MovementUserOneId
           ,@UserTwoId
           ,@MovementUserTwoId)
END
GO


