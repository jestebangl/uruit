SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertAndGetUserId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InsertAndGetUserId] AS' 
END
GO
ALTER PROCEDURE [dbo].[InsertAndGetUserId] @userName VARCHAR(50) 
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [User] WHERE UserName LIKE UPPER(@userName))
	BEGIN
		INSERT INTO [User](UserName) VALUES(UPPER(@userName));
	END

	SELECT IdUser FROM [User] WHERE UserName LIKE UPPER(@userName);
END
GO

