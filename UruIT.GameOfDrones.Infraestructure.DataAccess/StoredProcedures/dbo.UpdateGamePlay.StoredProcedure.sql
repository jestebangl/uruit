SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateGamePlay]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateGamePlay] AS' 
END
GO
ALTER PROCEDURE [dbo].[UpdateGamePlay] @IdUserWinner BIGINT,
									   @GamePlayId BIGINT
AS
BEGIN
	IF EXISTS(SELECT * FROM [dbo].[GamePlay] WHERE [GamePlayId]=@GamePlayId AND @IdUserWinner=[UserOne])
	BEGIN
		UPDATE [dbo].[GamePlay]
		SET [UserOneWins] = [UserOneWins] + 1
		WHERE [GamePlayId] = @GamePlayId;
	END
	ELSE IF EXISTS(SELECT * FROM [dbo].[GamePlay] WHERE [GamePlayId]=@GamePlayId AND @IdUserWinner=[UserOne])
	BEGIN
		UPDATE [dbo].[GamePlay]
		SET [UserTwoWins] = [UserTwoWins] + 1
		WHERE [GamePlayId] = @GamePlayId;
	END
	ELSE 
	BEGIN
		UPDATE [dbo].[GamePlay]
		SET [UserOneWins] = [UserOneWins] + 1,
			[UserTwoWins] = [UserTwoWins] + 1
		WHERE [GamePlayId] = @GamePlayId;
	END
END
GO

