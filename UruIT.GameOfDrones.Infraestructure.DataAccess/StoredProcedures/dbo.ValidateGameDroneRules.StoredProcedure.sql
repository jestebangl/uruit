SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidateGameDroneRules]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ValidateGameDroneRules] AS' 
END
GO
ALTER PROCEDURE [dbo].[ValidateGameDroneRules] @MovementUserOneId BIGINT,
												@MovementUserTwoId BIGINT
AS
BEGIN
	IF EXISTS(SELECT * FROM [dbo].[GameDronesRules] WHERE [WinAgainsToMovementId]=@MovementUserOneId AND [LoseAgainstToMovementId]=@MovementUserTwoId)
	BEGIN 
		SELECT @MovementUserOneId AS Winner,
			[Keyword] AS KeyWord
		FROM [GameDronesRules]
		WHERE [WinAgainsToMovementId]=@MovementUserOneId AND 
		[LoseAgainstToMovementId]=@MovementUserTwoId;
	END
	ELSE IF EXISTS(SELECT * FROM [dbo].[GameDronesRules] WHERE [WinAgainsToMovementId]=@MovementUserTwoId AND [LoseAgainstToMovementId]=@MovementUserOneId)
	BEGIN
		SELECT @MovementUserTwoId AS Winner,
			[Keyword] AS KeyWord
		FROM [GameDronesRules]
		WHERE [WinAgainsToMovementId]=@MovementUserTwoId AND 
		[LoseAgainstToMovementId]=@MovementUserOneId
	END
END
GO


