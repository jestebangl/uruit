SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetHistoricMatches]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetHistoricMatches] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetHistoricMatches] @RowCountPerPage INT,
   									  @PageIndex INT
AS
BEGIN
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
    SET DATEFORMAT DMY;

	WITH TempResult AS(

		SELECT GamePlayId ,
			u1.UserName AS Player1,
			[UserOneWins],
			u2.UserName AS Player2,
			[UserTwoWins],
			[GameDate]
			FROM [dbo].[GamePlay] gp
		INNER JOIN [dbo].[User] AS u1 ON (gp.[UserOne] = u1.[IdUser])
		INNER JOIN [dbo].[User] AS u2 ON (gp.[UserTwo] = u2.[IdUser])
	), TempCount AS (SELECT COUNT(*) AS TotalCount FROM TempResult)

	SELECT * FROM TempResult, TempCount
		  ORDER BY TempResult.[GameDate] DESC,
                TempResult.GamePlayId DESC
        OFFSET(@PageIndex - 1) * @RowCountPerPage ROWS FETCH NEXT @RowCountPerPage ROWS ONLY;	
END
GO

