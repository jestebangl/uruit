﻿namespace UruIT.GameOfDrones.Infraestructure.DataAccess.DataAccess
{
    using System.Configuration;
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces;

    public class UserDac : IUserDac
    {
        public IDataAccessObject Dao { get; set; }

        public UserDac(IDataAccessObject Dao)
        {
            this.Dao = Dao;
        }

        public DataSet InsertAndGetUser(UserModel usersModel)
        {
            var response = Dao.SqlQueryResult("InsertAndGetUserId",
                new ModelDao("userName", SqlDbType.VarChar, usersModel.UserName));

            return response;
        }


    }
}
