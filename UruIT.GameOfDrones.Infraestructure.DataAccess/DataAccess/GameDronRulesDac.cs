﻿namespace UruIT.GameOfDrones.Infraestructure.DataAccess.DataAccess
{
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces;
    using UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces;


    public class GameDronRulesDac : IGameDronRulesDac
    {
        public IDataAccessObject Dao { get; set; }

        public GameDronRulesDac(IDataAccessObject Dao)
        {
            this.Dao = Dao;
        }

        public DataSet ValidateGameDroneRules(GamePlayDetailModel gamePlayDetailModel)
        {
            var response = Dao.SqlQueryResult("ValidateGameDroneRules",
                new ModelDao("MovementUserOneId", SqlDbType.BigInt, gamePlayDetailModel.MovementUserOneId),
                new ModelDao("MovementUserTwoId", SqlDbType.BigInt, gamePlayDetailModel.MovementUserTwoId));

            return response;
        }
    }
}
