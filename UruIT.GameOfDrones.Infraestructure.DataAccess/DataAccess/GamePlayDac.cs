﻿namespace UruIT.GameOfDrones.Infraestructure.DataAccess.DataAccess
{
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces;


    public class GamePlayDac : IGamePlayDac
    {
        public IDataAccessObject Dao { get; set; }

        public GamePlayDac(IDataAccessObject Dao)
        {
            this.Dao = Dao;
        }

        public DataSet AddNewGamePlay(UserModel[] userModels)
        {
            var response = Dao.SqlQueryResult("InsertGamePlay",
                new ModelDao("IdUserOne", SqlDbType.BigInt, userModels[0].UserId),
                new ModelDao("IdUserTwo", SqlDbType.BigInt, userModels[1].UserId));
            return response;
        }

        public void AddNewGamePlayDetail(GamePlayDetailModel gamePlayDetailModel)
        {
            Dao.SqlUpdate("AddNewGamePlayDetail",
                new ModelDao("GameId", SqlDbType.Int, gamePlayDetailModel.GameId),
                new ModelDao("GamePlayId", SqlDbType.BigInt, gamePlayDetailModel.GamePlayId),
                new ModelDao("UserOneId", SqlDbType.BigInt, gamePlayDetailModel.UserOneId),
                new ModelDao("MovementUserOneId", SqlDbType.BigInt, gamePlayDetailModel.MovementUserOneId),
                new ModelDao("UserTwoId", SqlDbType.BigInt, gamePlayDetailModel.UserTwoId),
                new ModelDao("MovementUserTwoId", SqlDbType.BigInt, gamePlayDetailModel.MovementUserTwoId));
        }

        public void UpdateGamePlay(long gamePlayId, long winnerUserId)
        {
            Dao.SqlUpdate("UpdateGamePlay",
                new ModelDao("IdUserWinner", SqlDbType.BigInt, winnerUserId),
                new ModelDao("GamePlayId", SqlDbType.BigInt, gamePlayId));
        }

        public DataSet HasAPlayerWon(long gamePlayId)
        {
            var response = Dao.SqlQueryResult("HasAPlayerWon",
                new ModelDao("GamePlayId", SqlDbType.BigInt, gamePlayId)
                );
            return response;
        }

        public DataSet GetHistoricMatches(int rowCountPerPage, int pageIndex)
        {
            var response = Dao.SqlQueryResult("GetHistoricMatches",
                new ModelDao("RowCountPerPage", SqlDbType.Int, rowCountPerPage),
                new ModelDao("PageIndex", SqlDbType.Int, pageIndex));

            return response;

        }
    }
}
