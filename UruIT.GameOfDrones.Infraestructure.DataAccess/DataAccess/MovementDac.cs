﻿namespace UruIT.GameOfDrones.Infraestructure.DataAccess.DataAccess
{
    using System.Data;
    using UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces;


    public class MovementDac : IMovementDac
    {
        public IDataAccessObject Dao { get; set; }

        public MovementDac(IDataAccessObject Dao)
        {
            this.Dao = Dao;
        }

        public DataSet GetMovementList()
        {
            var response = Dao.SqlQueryResult("GetMovementList");
            return response;
        }
    }
}
