﻿namespace UruIT.GameOfDrones.Application.Operator.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using UruIT.GameOfDrones.Domain.Entities.Enum;
    using UruIT.GameOfDrones.Domain.Entities.Helper;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;

    public class MovementHelper : IMovementHelper
    {
        public MovementHelper() { }

        public List<MovementModel> GetMovementListFromDataSet(DataSet movementDataSet)
        {
            if (movementDataSet != null && movementDataSet.Tables[0].Rows.Count > 0)
            {
                var movementList = from row in movementDataSet.Tables[0].AsEnumerable()
                                   select new MovementModel()
                                   {
                                       MovementId = Convert.ToInt64(row["MovementId"]),
                                       MovementName = Convert.ToString(row["MovementName"])
                                   };

                return movementList.ToList();
            }

            throw new UruITException("GetMovementListFromDataSet", ErrorMessagesEnum.MovementsNotFound, null);
        }

    }
}
