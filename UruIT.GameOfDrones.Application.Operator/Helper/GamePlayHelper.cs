﻿namespace UruIT.GameOfDrones.Application.Operator.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using UruIT.GameOfDrones.Domain.Entities.Enum;
    using UruIT.GameOfDrones.Domain.Entities.Helper;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;

    public class GamePlayHelper : IGamePlayHelper
    {
        public GamePlayHelper() { }

        public GamePlayModel GetGamePlayIdFromDataSet(DataSet gamePlayDataSet)
        {
            if (gamePlayDataSet != null && gamePlayDataSet.Tables[0].Rows.Count > 0)
            {
                var gamePlayList = from row in gamePlayDataSet.Tables[0].AsEnumerable()
                                   select new GamePlayModel()
                                   {
                                       GamePlayId = Convert.ToInt64(row["GamePlayId"]),
                                       UserOne = Convert.ToInt64(row["UserOne"]),
                                       UserTwo = Convert.ToInt64(row["UserTwo"]),
                                       UserOneWins = Convert.ToInt64(row["UserOneWins"]),
                                       UserTwoWins = Convert.ToInt64(row["UserTwoWins"])
                                   };

                return gamePlayList.FirstOrDefault();
            }

            throw new UruITException("GetGamePlayIdFromDataSet", ErrorMessagesEnum.GamePlayIdNotFound, null);
        }

        public long GetTheMathcWinner(DataSet winnerResponse)
        {
            var winnerUserId = Convert.ToInt64(winnerResponse.Tables[0].Rows[0]["Winner"]);
            return winnerUserId;
        }

        public List<HistoricGamePlayModel> GetHistoricGamePlayModelFromDataSet(DataSet gamePlayDataSet)
        {
            if (gamePlayDataSet != null && gamePlayDataSet.Tables[0].Rows.Count > 0)
            {
                var historicList = from row in gamePlayDataSet.Tables[0].AsEnumerable()
                                   select new HistoricGamePlayModel()
                                   {
                                       GamePlayId = Convert.ToInt64(row["GamePlayId"]),
                                       Player1 = Convert.ToString(row["Player1"]),
                                       Player2 = Convert.ToString(row["Player2"]),
                                       UserOneWins = Convert.ToInt32(row["UserOneWins"]),
                                       UserTwoWins = Convert.ToInt32(row["UserTwoWins"]),
                                       GameDate = Convert.ToDateTime(row["GameDate"]),
                                   };

                return historicList.ToList();
            }

            throw new UruITException("GetHistoricGamePlayModelFromDataSet", ErrorMessagesEnum.HistoricFilesNotFound, null);
        }
    }
}
