﻿namespace UruIT.GameOfDrones.Application.Operator.Helper
{
    using System;
    using System.Data;
    using System.Linq;
    using UruIT.GameOfDrones.Domain.Entities.Enum;
    using UruIT.GameOfDrones.Domain.Entities.Helper;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;


    public class UserHelper : IUserHelper
    {
        public UserHelper()
        {
        }

        public UserModel GetUserFromDataSet(DataSet userDataSet, string userName)
        {
            if (userDataSet != null && userDataSet.Tables[0].Rows.Count > 0)
            {
                var userList = from row in userDataSet.Tables[0].AsEnumerable()
                               select new UserModel()
                               {
                                   UserId = Convert.ToInt64(row["IdUser"]),
                                   UserName = userName.ToUpper()
                               };

                return userList.FirstOrDefault();
            }

            throw new UruITException("GetUserFromDataSet", ErrorMessagesEnum.UserIdNorFound, null);
        }
    }
}
