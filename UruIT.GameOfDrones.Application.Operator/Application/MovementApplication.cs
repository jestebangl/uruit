﻿namespace UruIT.GameOfDrones.Application.Operator.Application
{
    using System.Collections.Generic;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;
    using UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces;


    public class MovementApplication : IMovementApplication
    {
        public IMovementDac MovementDac { get; set; }

        public IMovementHelper MovementHelper { get; set; }

        public MovementApplication(IMovementDac MovementDac, IMovementHelper MovementHelper)
        {
            this.MovementDac = MovementDac;
            this.MovementHelper = MovementHelper;
        }

        public List<MovementModel> GetMovementList()
        {
            var movementDataSet = MovementDac.GetMovementList();
            return MovementHelper.GetMovementListFromDataSet(movementDataSet);
        }



    }
}
