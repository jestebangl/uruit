﻿namespace UruIT.GameOfDrones.Application.Operator.Application
{
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;
    using UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces;


    public class UserApplication : IUserApplication
    {
        public IUserDac UserDac { get; set; }

        public IUserHelper UserHelper { get; set; }

        public UserApplication(IUserDac UserDac, IUserHelper UserHelper)
        {
            this.UserDac = UserDac;
            this.UserHelper = UserHelper;
        }

        public UserModel InsertAndGetUser(UserModel userModel)
        {
            var userIdDataSet = UserDac.InsertAndGetUser(userModel);
            return UserHelper.GetUserFromDataSet(userIdDataSet, userModel.UserName);
        }
    }
}
