﻿namespace UruIT.GameOfDrones.Application.Operator.Application
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using UruIT.GameOfDrones.Domain.Entities.Logger;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;
    using UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces;


    public class GamePlayApplication : IGamePlayApplication
    {
        public IGamePlayDac GamePlayDac { get; set; }
        public IGameDronRulesDac GameDronRulesDac { get; set; }
        public IGamePlayHelper GamePlayHelper { get; set; }

        public GamePlayApplication(IGamePlayDac GamePlayDac, IGameDronRulesDac GameDronRulesDac, IGamePlayHelper GamePlayHelper)
        {
            this.GamePlayDac = GamePlayDac;
            this.GameDronRulesDac = GameDronRulesDac;
            this.GamePlayHelper = GamePlayHelper;
        }

        public GamePlayModel AddNewGamePlay(UserModel[] userModels)
        {
            var response = GamePlayDac.AddNewGamePlay(userModels);
            return GamePlayHelper.GetGamePlayIdFromDataSet(response);
        }

        public long ProcessGameMovement(GamePlayDetailModel gamePlayDetailModel)
        {
            Logger.Current.Debug("Getting the winner.");
            var dataSet = GameDronRulesDac.ValidateGameDroneRules(gamePlayDetailModel);
            var winnerUIserId = GamePlayHelper.GetTheMathcWinner(dataSet);

            Logger.Current.Debug($"Game winner {winnerUIserId}.");

            Logger.Current.Debug($"Adding the detail of the game to the database");
            GamePlayDac.AddNewGamePlayDetail(gamePlayDetailModel);

            if (gamePlayDetailModel.MovementUserOneId != gamePlayDetailModel.MovementUserTwoId)
            {
                GamePlayDac.UpdateGamePlay(gamePlayDetailModel.GamePlayId, winnerUIserId);
            }
            else
            {
                winnerUIserId = 0;
            }


            return winnerUIserId;
        }

        public bool HasAPlayerWon(GamePlayDetailModel gamePlayDetailModel)
        {
            var gamePlayDataSet = GamePlayDac.HasAPlayerWon(gamePlayDetailModel.GamePlayId);

            var winsGoal = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfGamesPerMatch"].ToString());

            var playsWonByUserOne = Convert.ToInt64(gamePlayDataSet.Tables[0].Rows[0]["UserOneWins"]);
            var playsWonByUserTwo = Convert.ToInt64(gamePlayDataSet.Tables[0].Rows[0]["UserTwoWins"]);

            return playsWonByUserOne == winsGoal || playsWonByUserTwo == winsGoal;
        }

        public List<HistoricGamePlayModel> GetHistoricGamePlay(int rowCountPerPage, int pageIndex)
        {
            var historicDataSet = GamePlayDac.GetHistoricMatches(rowCountPerPage, pageIndex);
            return GamePlayHelper.GetHistoricGamePlayModelFromDataSet(historicDataSet);
        }



    }
}
