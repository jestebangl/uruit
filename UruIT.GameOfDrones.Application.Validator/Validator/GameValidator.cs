﻿namespace UruIT.GameOfDrones.Application.Validator.Validator
{
    using FluentValidation.Results;
    using System;
    using System.Linq;
    using UruIT.GameOfDrones.Domain.Entities.Logger;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;

    public class GameValidator : IGameValidator
    {
        public GameValidator() { }

        /// <summary>
        /// Executes the validate game model.
        /// </summary>
        /// <param name="gameModel">The game model.</param>
        public void ExecuteValidateGameModel(GamePlayDetailModel gameModel)
        {
            var validator = new GameGeneralValidator();
            var validationResult = validator.Validate(gameModel);
            ValidateErrors(validationResult);
        }

        /// <summary>
        /// Validates the errors.
        /// </summary>
        /// <param name="resultGameModel">The result game model.</param>
        /// <exception cref="System.ApplicationException"></exception>
        private void ValidateErrors(ValidationResult resultGameModel)
        {
            if (!resultGameModel.IsValid)
            {
                Logger.Current.Error("The game is not valid, returning de validtion errors At Application.ValidateErrors");

                var errorsDetail = string.Empty;
                resultGameModel.Errors.All(e => { errorsDetail += e.ErrorMessage + " | "; return true; });
                throw new ApplicationException(errorsDetail);
            }
        }
    }
}
