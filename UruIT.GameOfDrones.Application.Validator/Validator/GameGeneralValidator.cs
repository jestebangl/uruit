﻿namespace UruIT.GameOfDrones.Application.Validator.Validator
{
    using FluentValidation;
    using UruIT.GameOfDrones.Domain.Entities.Enum;
    using UruIT.GameOfDrones.Domain.Entities.Models;


    public class GameGeneralValidator : AbstractValidator<GamePlayDetailModel>
    {
        public GameGeneralValidator()
        {
            this.CascadeMode = CascadeMode.StopOnFirstFailure;
            this.RuleFor(gameModel => gameModel.MovementUserOneId).NotNull().WithMessage(ErrorMessagesEnum.ValueCanNotBeNull.ToDescriptionString());
            this.RuleFor(gameModel => gameModel.MovementUserOneId).NotEmpty().WithMessage(ErrorMessagesEnum.ValueCanNotBeEmpty.ToDescriptionString());
            this.RuleFor(gameModel => gameModel.MovementUserTwoId).NotNull().WithMessage(ErrorMessagesEnum.ValueCanNotBeNull.ToDescriptionString());
            this.RuleFor(gameModel => gameModel.MovementUserTwoId).NotEmpty().WithMessage(ErrorMessagesEnum.ValueCanNotBeNull.ToDescriptionString());
        }

    }
}
