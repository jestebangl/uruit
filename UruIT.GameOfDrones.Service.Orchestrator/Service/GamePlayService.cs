﻿namespace UruIT.GameOfDrones.Service.Orchestrator.Service
{
    using System;
    using System.Collections.Generic;
    using UruIT.GameOfDrones.Domain.Entities.Enum;
    using UruIT.GameOfDrones.Domain.Entities.Helper;
    using UruIT.GameOfDrones.Domain.Entities.Logger;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;
    using UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces;

    public class GamePlayService : IGamePlayService
    {
        public IGamePlayApplication GamePlayApplication { get; set; }

        public IGameValidator GameValidator { get; set; }

        public GamePlayService(IGamePlayApplication GamePlayApplication, IGameValidator GameValidator)
        {
            this.GamePlayApplication = GamePlayApplication;
            this.GameValidator = GameValidator;
        }

        public GamePlayModel AddNewGamePlay(UserModel[] userModels)
        {
            try
            {
                return GamePlayApplication.AddNewGamePlay(userModels);
            }
            catch (UruITException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new UruITException("AddNewGamePlay", ErrorMessagesEnum.UnexpectedError, ex);
            }
        }

        public long ProcessGameMovement(GamePlayDetailModel gamePlayDetailModel)
        {
            try
            {
                Logger.Current.Debug("Validating object");
                GameValidator.ExecuteValidateGameModel(gamePlayDetailModel);
                Logger.Current.Debug("Validations correct.");
                Logger.Current.Debug("Verifying game rules.");
                long response = GamePlayApplication.ProcessGameMovement(gamePlayDetailModel);
                return response;
            }
            catch (UruITException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new UruITException("ProcessGameMovement", ErrorMessagesEnum.UnexpectedError, ex);
            }
        }

        public bool HasAPlayerWon(GamePlayDetailModel gamePlayDetailModel)
        {
            try
            {
                return GamePlayApplication.HasAPlayerWon(gamePlayDetailModel);
            }
            catch (UruITException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new UruITException("HasAPlayerWon", ErrorMessagesEnum.UnexpectedError, ex);
            }
        }

        public List<HistoricGamePlayModel> GetHistoricGamePlay(int rowCountPerPage, int pageIndex)
        {
            try
            {
                return GamePlayApplication.GetHistoricGamePlay(rowCountPerPage, pageIndex);
            }
            catch (UruITException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new UruITException("GetHistoricGamePlay", ErrorMessagesEnum.UnexpectedError, ex);
            }
        }
    }
}
