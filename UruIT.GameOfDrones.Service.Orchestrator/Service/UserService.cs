﻿
namespace UruIT.GameOfDrones.Service.Orchestrator.Service
{
    using System;
    using UruIT.GameOfDrones.Domain.Entities.Enum;
    using UruIT.GameOfDrones.Domain.Entities.Helper;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;
    using UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces;


    public class UserService : IUserService
    {
        public IUserApplication UserApplication { get; set; }

        public UserService(IUserApplication UserApplication)
        {
            this.UserApplication = UserApplication;
        }

        public UserModel InsertAndGetUser(UserModel userModel)
        {
            try
            {
                return UserApplication.InsertAndGetUser(userModel);
            }
            catch (UruITException uruIT)
            {
                throw uruIT;
            }
            catch (Exception ex)
            {
                throw new UruITException("InsertAndGetUser", ErrorMessagesEnum.UnexpectedError, ex);
            }
        }
    }
}
