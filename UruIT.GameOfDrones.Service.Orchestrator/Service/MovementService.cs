﻿namespace UruIT.GameOfDrones.Service.Orchestrator.Service
{
    using System;
    using System.Collections.Generic;
    using UruIT.GameOfDrones.Domain.Entities.Enum;
    using UruIT.GameOfDrones.Domain.Entities.Helper;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces;
    using UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces;

    public class MovementService : IMovementService
    {
        public IMovementApplication MovementApplication { get; set; }

        public MovementService(IMovementApplication MovementApplication)
        {
            this.MovementApplication = MovementApplication;
        }

        public List<MovementModel> GetMovementList()
        {
            try
            {
                return MovementApplication.GetMovementList();
            }
            catch (UruITException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new UruITException("GetMovementList", ErrorMessagesEnum.UnexpectedError, ex);
            }
        }
    }
}
