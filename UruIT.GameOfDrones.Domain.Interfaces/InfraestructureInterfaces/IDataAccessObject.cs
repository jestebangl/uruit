﻿namespace UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces
{
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;

    public interface IDataAccessObject
    {
        /// <summary>
        /// Data access object method to return a sql query result from the stored procedure executed
        /// </summary>
        /// <param name="sqlProcedueName">Stored procedure name</param>
        /// <param name="parameters">parameters to send to the stored procedure</param>
        /// <returns>Dataset with a table for each different select from the stored procedure</returns>
        DataSet SqlQueryResult(string sqlProcedueName, params ModelDao[] parameters);

        /// <summary>
        /// Data access object method usued to return true or false for insert, update and delete DML operations
        /// </summary>
        /// <param name="sqlProcedueName">Stored procedure name</param>
        /// <param name="parameters">parameters to send to the stored procedure</param>
        /// <returns>true if the stores procedure executions was correct, false i</returns>
        bool SqlUpdate(string sqlProcedueName, params ModelDao[] parameters);
    }
}
