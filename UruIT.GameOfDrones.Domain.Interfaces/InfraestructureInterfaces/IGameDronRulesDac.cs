﻿namespace UruIT.GameOfDrones.Domain.Interfaces
{
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;


    public interface IGameDronRulesDac
    {
        /// <summary>
        /// Validate the winner of the match
        /// </summary>
        /// <param name="gamePlayDetailModel"></param>
        /// <returns></returns>
        DataSet ValidateGameDroneRules(GamePlayDetailModel gamePlayDetailModel);
    }
}
