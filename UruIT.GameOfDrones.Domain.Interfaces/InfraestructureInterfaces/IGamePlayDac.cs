﻿namespace UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces
{
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;

    public interface IGamePlayDac
    {
        /// <summary>
        /// Add new Game Play
        /// </summary>
        /// <returns>Data Set with the GamePlay Id</returns>
        DataSet AddNewGamePlay(UserModel[] userModels);

        /// <summary>
        /// Add new game play detail with the movement selected for each player
        /// </summary>
        /// <param name="gamePlayDetailModel"></param>
        void AddNewGamePlayDetail(GamePlayDetailModel gamePlayDetailModel);

        /// <summary>
        /// Updates the Game Play
        /// </summary>
        /// <param name="gamePlayId">Game Play Id</param>
        /// <param name="winnerUserId">USerId who wins the match</param>
        void UpdateGamePlay(long gamePlayId, long winnerUserId);

        /// <summary>
        /// Get the Game Play record to verufy si a player has already won
        /// </summary>
        /// <param name="gamePlayId">GamePlayId to the current match</param>
        /// <returns></returns>
        DataSet HasAPlayerWon(long gamePlayId);

        /// <summary>
        /// Get the Historic Game Plays
        /// </summary>
        /// <param name="rowCountPerPage">Rows per page</param>
        /// <param name="pageIndex">Pagen Number</param>
        /// <returns></returns>
        DataSet GetHistoricMatches(int rowCountPerPage, int pageIndex);
    }
}
