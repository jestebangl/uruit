﻿namespace UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces
{
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;


    public interface IUserDac
    {
        /// <summary>
        /// Insert the username if not exist and return a DataSet with the User Id
        /// </summary>
        /// <param name="usersModel">User model with username to verify</param>
        /// <returns></returns>
        DataSet InsertAndGetUser(UserModel usersModel);
    }
}
