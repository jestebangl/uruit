﻿namespace UruIT.GameOfDrones.Domain.Interfaces.InfraestructureInterfaces
{
    using System.Data;


    public interface IMovementDac
    {
        /// <summary>
        /// Returns a DataSet with the list of Movements in the database.
        /// </summary>
        /// <returns></returns>
        DataSet GetMovementList();
    }
}
