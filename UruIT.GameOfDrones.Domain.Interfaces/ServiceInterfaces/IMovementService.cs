﻿namespace UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces
{
    using System.Collections.Generic;
    using UruIT.GameOfDrones.Domain.Entities.Models;


    public interface IMovementService
    {
        /// <summary>
        /// Gets the Movemen List from the db
        /// </summary>
        /// <returns>List of MovementModel</returns>
        List<MovementModel> GetMovementList();
    }
}
