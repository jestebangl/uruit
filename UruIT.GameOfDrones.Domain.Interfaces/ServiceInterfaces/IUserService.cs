﻿using UruIT.GameOfDrones.Domain.Entities.Models;

namespace UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces
{
    public interface IUserService
    {
        UserModel InsertAndGetUser(UserModel userModel);
    }
}
