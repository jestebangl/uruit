﻿namespace UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces
{
    using System.Collections.Generic;
    using UruIT.GameOfDrones.Domain.Entities.Models;


    public interface IGamePlayService
    {
        GamePlayModel AddNewGamePlay(UserModel[] userModels);

        long ProcessGameMovement(GamePlayDetailModel gamePlayDetailModel);

        bool HasAPlayerWon(GamePlayDetailModel gamePlayDetailModel);

        List<HistoricGamePlayModel> GetHistoricGamePlay(int rowCountPerPage, int pageIndex);
    }
}
