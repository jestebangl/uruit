﻿namespace UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces
{
    using UruIT.GameOfDrones.Domain.Entities.Models;

    public interface IGameValidator
    {
        /// <summary>
        /// Executes the validate game model.
        /// </summary>
        /// <param name="game">The game.</param>
        void ExecuteValidateGameModel(GamePlayDetailModel gamePlayDetailModel);
    }

}
