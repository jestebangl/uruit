﻿namespace UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces
{
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;

    public interface IUserHelper
    {
        /// <summary>
        /// Get the user model given a data set with the id.s
        /// </summary>
        /// <param name="userDataSet"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        UserModel GetUserFromDataSet(DataSet userDataSet, string userName);
    }
}
