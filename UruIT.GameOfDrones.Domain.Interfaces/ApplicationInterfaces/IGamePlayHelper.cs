﻿namespace UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces
{
    using System.Collections.Generic;
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;


    public interface IGamePlayHelper
    {
        /// <summary>
        /// Get the game play id from data set
        /// </summary>
        /// <param name="gamePlayDataSet">DataSet from database with the game play id</param>
        /// <returns>GamePlayModel with the current game play id</returns>
        GamePlayModel GetGamePlayIdFromDataSet(DataSet gamePlayDataSet);

        /// <summary>
        /// Gets the winner for the current match
        /// </summary>
        /// <param name="winnerResponse"></param>
        /// <param name="gamePlayDetailModel"></param>
        /// <returns></returns>
        long GetTheMathcWinner(DataSet winnerResponse);

        /// <summary>
        /// Get the list of the game plays
        /// </summary>
        /// <param name="gamePlayDataSet"></param>
        /// <returns>List of HistoricGamePlayModel</returns>
        List<HistoricGamePlayModel> GetHistoricGamePlayModelFromDataSet(DataSet gamePlayDataSet);
    }
}
