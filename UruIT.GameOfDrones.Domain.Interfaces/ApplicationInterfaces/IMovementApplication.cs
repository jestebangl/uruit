﻿namespace UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces
{
    using System.Collections.Generic;
    using UruIT.GameOfDrones.Domain.Entities.Models;


    public interface IMovementApplication
    {
        /// <summary>
        /// Get the movement list from the database
        /// </summary>
        /// <returns>List of MovementModel</returns>
        List<MovementModel> GetMovementList();
    }
}
