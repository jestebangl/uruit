﻿namespace UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces
{
    using System.Collections.Generic;
    using System.Data;
    using UruIT.GameOfDrones.Domain.Entities.Models;


    public interface IMovementHelper
    {
        /// <summary>
        /// Return a list of MovementModle given a DataSet with db records
        /// </summary>
        /// <param name="movementDataSet">DataSet with db records</param>
        /// <returns>List of MovementModel</returns>
        List<MovementModel> GetMovementListFromDataSet(DataSet movementDataSet);
    }
}
