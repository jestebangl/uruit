﻿namespace UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces
{
    using System.Collections.Generic;
    using UruIT.GameOfDrones.Domain.Entities.Models;

    public interface IGamePlayApplication
    {
        /// <summary>
        /// Adds a new game play into the data base
        /// </summary>
        /// <returns>GamePlayId from the current GamePlay</returns>
        GamePlayModel AddNewGamePlay(UserModel[] userModels);

        /// <summary>
        /// Process the new winner for the current match
        /// </summary>
        /// <param name="gamePlayDetailModel"></param>
        /// <returns>User Id of the winner</returns>
        long ProcessGameMovement(GamePlayDetailModel gamePlayDetailModel);

        /// <summary>
        /// Returns if a player has already won
        /// </summary>
        /// <param name="gamePlayDetailModel"></param>
        /// <returns>True if a player won, false if is not</returns>
        bool HasAPlayerWon(GamePlayDetailModel gamePlayDetailModel);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rowCountPerPage"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        List<HistoricGamePlayModel> GetHistoricGamePlay(int rowCountPerPage, int pageIndex);

    }
}
