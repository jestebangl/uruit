﻿using UruIT.GameOfDrones.Domain.Entities.Models;

namespace UruIT.GameOfDrones.Domain.Interfaces.ApplicationInterfaces
{
    public interface IUserApplication
    {
        /// <summary>
        /// Return user model with user id and user name
        /// </summary>
        /// <param name="userModel">UserMolde only with the UserName</param>
        /// <returns>UserModel with UserId and UserName</returns>
        UserModel InsertAndGetUser(UserModel userModel);
    }
}
