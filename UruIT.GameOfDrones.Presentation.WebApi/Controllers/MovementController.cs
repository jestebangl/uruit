﻿namespace UruIT.GameOfDrones.Presentation.WebApi.Controllers
{
    using System.Web.Http;
    using UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces;

    public class MovementController : ApiController
    {
        public IMovementService MovementService { get; set; }

        public MovementController(IMovementService MovementService)
        {
            this.MovementService = MovementService;
        }

        [HttpGet]
        [Route("api/movement/getmovementlist")]
        public IHttpActionResult GetMovementList()
        {
            return Ok(MovementService.GetMovementList());
        }
    }
}
