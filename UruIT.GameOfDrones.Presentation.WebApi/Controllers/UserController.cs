﻿namespace UruIT.GameOfDrones.Presentation.WebApi.Controllers
{
    using System.Web.Http;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces;


    public class UserController : ApiController
    {
        public IUserService UserService { get; set; }

        public UserController(IUserService UserService)
        {
            this.UserService = UserService;
        }

        [HttpPost]
        [Route("api/user/insertandgetuser")]
        public IHttpActionResult InsertAndGetUser(UserModel userModel)
        {
            return Ok(UserService.InsertAndGetUser(userModel));
        }
    }
}
