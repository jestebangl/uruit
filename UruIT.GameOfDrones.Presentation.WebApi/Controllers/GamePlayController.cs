﻿namespace UruIT.GameOfDrones.Presentation.WebApi.Controllers
{
    using System.Web.Http;
    using UruIT.GameOfDrones.Domain.Entities.Models;
    using UruIT.GameOfDrones.Domain.Interfaces.ServiceInterfaces;


    public class GamePlayController : ApiController
    {
        public IGamePlayService GamePlayService { get; set; }

        public GamePlayController(IGamePlayService GamePlayService)
        {
            this.GamePlayService = GamePlayService;
        }

        [HttpPost]
        [Route("api/gameplay/addnewgameplay/")]
        public IHttpActionResult AddNewGamePlay(UserModel[] userModels)
        {
            return Ok(GamePlayService.AddNewGamePlay(userModels));
        }

        [HttpPost]
        [Route("api/gameplay/processgamemovement/")]
        public IHttpActionResult ProcessGameMovement(GamePlayDetailModel gamePlayDetailModel)
        {
            return Ok(GamePlayService.ProcessGameMovement(gamePlayDetailModel));
        }

        [HttpPost]
        [Route("api/gameplay/hasaplayerwon/")]
        public IHttpActionResult HasAPlayerWon(GamePlayDetailModel gamePlayDetailModel)
        {
            return Ok(GamePlayService.HasAPlayerWon(gamePlayDetailModel));
        }

        [HttpGet]
        [Route("api/gameplay/gethistoricgameplay/")]
        public IHttpActionResult GetHistoricGamePlay(int rowCountPerPage, int pageIndex)
        {
            return Ok(GamePlayService.GetHistoricGamePlay(rowCountPerPage, pageIndex));
        }
    }
}
