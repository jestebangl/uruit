﻿namespace UruIT.GameOfDrones.Presentation.WebApi.Controllers
{
    using System.Configuration;
    using System.Web.Http;


    public class ConfigurationController : ApiController
    {
        public ConfigurationController() { }

        [HttpGet]
        [Route("api/configuration/getnumerofmatches")]
        public IHttpActionResult GetNumerOfMatches()
        {
            return Ok(ConfigurationManager.AppSettings["NumberOfGamesPerMatch"].ToString());
        }
    }
}
